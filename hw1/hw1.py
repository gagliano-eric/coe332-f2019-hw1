import csv
import math

def read_data():
	with open('rainfall.csv','r') as input:
		reader = csv.DictReader(input)
		listdict = list([dict([a,convert(b)] for a,b in line.items()) for line in reader])
		return listdict

def convert(val):
	constructors = [int, float, str]
	for c in constructors:
		try:
			return c(val)
		except ValueError:
			pass

def dates(data, start=None, end=None):
	list = data
	if start==None:
		start = -math.inf
	if end==None:
		end = math.inf
	newlist = [dic for dic in list if not (dic['year'] < start or dic['year'] > end)]
	return newlist

def paginate(data, offset=None, limit=None):
	list = data
	counter = 0
	newlist = []
	if offset==None:
		offset = 0
	if limit==None:
		limit = math.inf
	for i in range(offset, len(list)):
		if counter < limit:
			counter = counter + 1
			newlist.append(list[i])
		else:
			break
	return newlist
